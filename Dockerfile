FROM maven:3.5-jdk-8-alpine
WORKDIR /app
COPY . /app
RUN mvn clean install

FROM openjdk:8-jre
WORKDIR /app
VOLUME ["/app"]
COPY --from=0 /app/target/app.jar /app
COPY --from=0 /app/start.sh start.sh
COPY --from=0 /app/wait-for-it.sh wait-for-it.sh
#COPY --from=0 /app/dump.sql /docker-entrypoint-initdb.d/
ENTRYPOINT ["java","-Xmx256m","-jar","app.jar"]