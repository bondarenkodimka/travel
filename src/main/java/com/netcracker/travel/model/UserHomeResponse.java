package com.netcracker.travel.model;

public class UserHomeResponse {

    private String nickname;
    private String name;
    private String surname;
    private Boolean googleConnect;
    private Boolean facebookConnect;
    private Boolean vkConnect;

    public UserHomeResponse(String nickname, String name, String surname, String googleIdConnect, String facebookIdConnect, String vkIdConnect) {
        this.nickname = nickname;
        this.name = name;
        this.surname = surname;
        this.googleConnect = googleIdConnect != null;
        this.facebookConnect = facebookIdConnect != null;
        this.vkConnect = vkIdConnect != null;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Boolean getGoogleConnect() {
        return googleConnect;
    }

    public void setGoogleConnect(Boolean googleConnect) {
        this.googleConnect = googleConnect;
    }

    public Boolean getFacebookConnect() {
        return facebookConnect;
    }

    public void setFacebookConnect(Boolean facebookConnect) {
        this.facebookConnect = facebookConnect;
    }

    public Boolean getVkConnect() {
        return vkConnect;
    }

    public void setVkConnect(Boolean vkConnect) {
        this.vkConnect = vkConnect;
    }

    @Override
    public String toString() {
        return "UserResponse{" +
                "nickname='" + nickname + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", googleConnect=" + googleConnect +
                ", facebookConnect=" + facebookConnect +
                ", vkConnect=" + vkConnect +
                '}';
    }
}
