package com.netcracker.travel.entities;

public class DialogEntity {
    private String senderId;
    private String senderName;
    private String recevierId;
    private String recevierName;
    private String firstname;
    private String lastname;
    private String photoUrl;
    private String lastMsg;

    public DialogEntity(String senderId, String senderName, String recevierId, String recevierName, String firstname, String lastname, String photoUrl, String lastMsg) {
        this.senderId = senderId;
        this.senderName = senderName;
        this.recevierId = recevierId;
        this.recevierName = recevierName;
        this.firstname = firstname;
        this.lastname = lastname;
        this.photoUrl = photoUrl;
        this.lastMsg = lastMsg;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getRecevierId() {
        return recevierId;
    }

    public void setRecevierId(String recevierId) {
        this.recevierId = recevierId;
    }

    public String getRecevierName() {
        return recevierName;
    }

    public void setRecevierName(String recevierName) {
        this.recevierName = recevierName;
    }

    public String getLastMsg() {
        return lastMsg;
    }

    public void setLastMsg(String lastMsg) {
        this.lastMsg = lastMsg;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
