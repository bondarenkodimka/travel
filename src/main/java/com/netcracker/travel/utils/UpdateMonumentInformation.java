package com.netcracker.travel.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.netcracker.travel.clients.GooglePlacesClient;
import com.netcracker.travel.entities.CityEntity;
import com.netcracker.travel.entities.CountryEntity;
import com.netcracker.travel.entities.MonumentEntity;
import com.netcracker.travel.entities.StatusMonumentEntity;
import com.netcracker.travel.repositories.CityRepository;
import com.netcracker.travel.repositories.CountryRepository;
import com.netcracker.travel.repositories.MonumentRepository;
import com.netcracker.travel.repositories.StatusMonumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UpdateMonumentInformation {

    @Autowired
    private CityRepository cityRepo;
    @Autowired
    private CountryRepository countryRepo;
    @Autowired
    private StatusMonumentRepository statusRepo;
    @Autowired
    private MonumentRepository sightRepo;
    @Autowired
    private GooglePlacesClient client;

    public void updateMonumentInformation() {
        CountryEntity russia = countryRepo.findByNameCountry("Russia");
        Iterable<CityEntity> cities = cityRepo.findAllByCountry(russia);
        Iterable<StatusMonumentEntity> statuses = statusRepo.findAll();
        int count = -1;
        for (CityEntity city : cities) {
            count++;
            if (city.getNameCity().equals("Volzhskiy") || city.getNameCity().equals("Volzhsk") || city.getNameCity().equals("Vol'sk") ||
                    city.getNameCity().equals("Vologda") ||city.getNameCity().equals("Volgograd") ||city.getNameCity().equals("Volgodonsk") ||city.getNameCity().equals("Vladimir") ||
                    city.getNameCity().equals("Vladikavkaz")||city.getNameCity().equals("Vidnoye") ||city.getNameCity().equals("Velikiye Luki")||city.getNameCity().equals("Uzlovaya")||
                    city.getNameCity().equals("Ulyanovsk")||city.getNameCity().equals("Ukhta") ||city.getNameCity().equals("Ufa")) {

                for (StatusMonumentEntity status : statuses) {
                    try {
                        List<MonumentInfo> sight = client.getSight(city.getLatitude().toString(), city.getLongitude().toString(), status.getNameStatus());
                        for (MonumentInfo rSight : sight) {
                            MonumentEntity saveEntity = new MonumentEntity(city, rSight.getName(), Double.parseDouble(rSight.getLng()), Double.parseDouble(rSight.getLat()), rSight.getPlaceId(), status, rSight.getMonumentInfo());
                            sightRepo.save(saveEntity);
                        }
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
