package com.netcracker.travel.utils;

public class MonumentInfo {
    private String name;
    private String lat;
    private String lng;
    private String placeId;
    private String monumentInfo;

    public MonumentInfo() {
    }

    public MonumentInfo(String name, String lat, String lng, String placeId, String monumentInfo) {
        this.name = name;
        this.lat = lat;
        this.lng = lng;
        this.placeId = placeId;
        this.monumentInfo = monumentInfo;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }


    public String getMonumentInfo() {
        return monumentInfo;
    }

    public void setMonumentInfo(String monumentInfo) {
        this.monumentInfo = monumentInfo;
    }

    @Override
    public String toString() {
        return "MonumentInfo{" +
                "name='" + name + '\'' +
                ", lat='" + lat + '\'' +
                ", lng='" + lng + '\'' +
                ", placeId='" + placeId + '\'' +
                '}';
    }
}
