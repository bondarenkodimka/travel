package com.netcracker.travel.utils;

import java.util.List;

public class HotelAllPhotoResponse {
    double longitude;
    double latitude;
    String nameHotel;
    double priceAvg;
    double avgPrice;
    int stars;
    double rating;
    String site;
    String formattedAddress;
    String formattedPhoneNumber;
    List<String> photo;

    public double getLongitude() {
        return longitude;
    }

    public HotelAllPhotoResponse setLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }

    public double getLatitude() {
        return latitude;
    }

    public HotelAllPhotoResponse setLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public String getNameHotel() {
        return nameHotel;
    }

    public HotelAllPhotoResponse setNameHotel(String nameHotel) {
        this.nameHotel = nameHotel;
        return this;
    }

    public double getPriceAvg() {
        return priceAvg;
    }

    public HotelAllPhotoResponse setPriceAvg(double priceAvg) {
        this.priceAvg = priceAvg;
        return this;
    }

    public double getAvgPrice() {
        return avgPrice;
    }

    public HotelAllPhotoResponse setAvgPrice(double avgPrice) {
        this.avgPrice = avgPrice;
        return this;
    }

    public int getStars() {
        return stars;
    }

    public HotelAllPhotoResponse setStars(int stars) {
        this.stars = stars;
        return this;
    }

    public double getRating() {
        return rating;
    }

    public HotelAllPhotoResponse setRating(double rating) {
        this.rating = rating;
        return this;
    }

    public String getSite() {
        return site;
    }

    public HotelAllPhotoResponse setSite(String site) {
        this.site = site;
        return this;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public HotelAllPhotoResponse setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
        return this;
    }

    public String getFormattedPhoneNumber() {
        return formattedPhoneNumber;
    }

    public HotelAllPhotoResponse setFormattedPhoneNumber(String formattedPhoneNumber) {
        this.formattedPhoneNumber = formattedPhoneNumber;
        return this;
    }

    public List<String> getPhoto() {
        return photo;
    }

    public HotelAllPhotoResponse setPhoto(List<String> photo) {
        this.photo = photo;
        return this;
    }

    public HotelAllPhotoResponse() {
    }

    public HotelAllPhotoResponse(double longitude, double latitude, String nameHotel,
                                 double priceAvg, double avgPrice, int stars,
                                 double rating, String site, String formattedAddress,
                                 String formattedPhoneNumber, List<String> photo) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.nameHotel = nameHotel;
        this.priceAvg = priceAvg;
        this.avgPrice = avgPrice;
        this.stars = stars;
        this.rating = rating;
        this.site = site;
        this.formattedAddress = formattedAddress;
        this.formattedPhoneNumber = formattedPhoneNumber;
        this.photo = photo;
    }
}
