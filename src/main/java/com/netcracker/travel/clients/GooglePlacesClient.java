package com.netcracker.travel.clients;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netcracker.travel.utils.Day;
import com.netcracker.travel.utils.MonumentInfo;
import com.netcracker.travel.utils.MonumentWorkTimeAndPhoto;
import com.netcracker.travel.utils.MonumentWorkTimeInfo;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Component
public class GooglePlacesClient {

    public List<MonumentInfo> getSight(String latitude, String longitude, String statusName) throws JsonProcessingException {
        final String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + latitude + "," + longitude
                + "&radius=5000&type=" + statusName + "&key=AIzaSyB-eceCpqDgw9ate_IDpSEOS9_8gibw9do";
        List<MonumentInfo> listSight = new ArrayList<>();
        RestTemplate restTemplate = new RestTemplate();
        String forObject = restTemplate.getForObject(url, String.class);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode results = mapper.readTree(forObject).get("results");
        if (results.isArray()) {
            for (JsonNode node : results) {
                JsonNode name = node.get("name");
                JsonNode location = node.get("geometry").get("location");
                JsonNode placeId = node.get("place_id");
                MonumentInfo sight = new MonumentInfo(name.toString(), location.get("lat").toString(), location.get("lng").toString(), placeId.toString(), descriptionMonument(name.toString().substring(1, name.toString().length() - 1)));
                listSight.add(sight);
            }
        }
        return listSight;
    }

    public MonumentWorkTimeAndPhoto getDetailsForMonuments(String placeId) throws JsonProcessingException, ParseException {
        final String url = "https://maps.googleapis.com/maps/api/place/details/json?place_id=" + placeId + "&fields=name,opening_hours,photo&language=eng&key=AIzaSyB-eceCpqDgw9ate_IDpSEOS9_8gibw9do";
        List<MonumentWorkTimeInfo> listSightDetails = new ArrayList<>();
        List<String> listSightPhoto = new ArrayList<>();
        Day[] days = Day.values();
        RestTemplate restTemplate = new RestTemplate();
        String forObject = restTemplate.getForObject(url, String.class);
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode results = mapper.readTree(forObject).get("result");
            JsonNode photo = results.get("photos");
            if (photo != null)
                for (JsonNode photoMonument : photo) {
                    listSightPhoto.add(photoMonument.get("photo_reference").toString());
                }
            else {
                listSightPhoto = null;
            }
            JsonNode openHours = results.get("opening_hours");
            if (openHours != null) {
                JsonNode openNow = openHours.get("open_now");
                JsonNode weekDay = openHours.get("periods");
                for (JsonNode obj : weekDay) {
                    JsonNode openDay = obj.get("open").get("day");
                    JsonNode openTime = obj.get("open").get("time");
                    if (obj.get("close") != null) {
                        JsonNode closeTime = obj.get("close").get("time");
                        listSightDetails.add(new MonumentWorkTimeInfo(days[openDay.asInt()], changeTime(openTime.asInt()), changeTime(closeTime.asInt()),
                                openNow.asBoolean()));
                    } else {
                        for (Day day : days)
                            listSightDetails.add(new MonumentWorkTimeInfo(day, changeTime(openTime.asInt()), changeTime(2400),
                                    openNow.asBoolean()));
                    }
                }
                if (listSightPhoto != null)
                    return new MonumentWorkTimeAndPhoto(listSightDetails, listSightPhoto);
                else
                    return new MonumentWorkTimeAndPhoto(listSightDetails, null);
            } else if (listSightPhoto != null)
                return new MonumentWorkTimeAndPhoto(null, listSightPhoto);
            else
                return new MonumentWorkTimeAndPhoto(null, null);
        }catch (Exception e){
            return null;
        }
    }

    private Time changeTime(int time) throws ParseException {
        String rezTime = "";
        if (time % 100 == 0)
            rezTime = rezTime + time / 100 + ":" + time % 100 + time % 10;
        else
            rezTime = rezTime + time / 100 + ":" + time % 100;
        DateFormat formatter = new SimpleDateFormat("HH:mm");
        java.sql.Time timeValue = new java.sql.Time(formatter.parse(rezTime).getTime());
        return timeValue;
    }


    public String descriptionMonument(String name) throws JsonProcessingException {
        final String url = "https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro=&explaintext=&titles=" + name;
        RestTemplate restTemplate = new RestTemplate();
        String forObject = restTemplate.getForObject(url, String.class);
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode query = mapper.readTree(forObject).get("query");
            JsonNode extract = query.findValue("extract");
            if (extract != null)
                return extract.toString();
            else
                return null;
        } catch (Exception e){
            return null;
        }
    }

}
