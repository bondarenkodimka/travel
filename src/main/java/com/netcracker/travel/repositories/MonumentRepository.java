package com.netcracker.travel.repositories;

import com.netcracker.travel.entities.CityEntity;
import com.netcracker.travel.entities.MonumentEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MonumentRepository extends CrudRepository<MonumentEntity, String> {
    List<MonumentEntity> findAllByCityMonument(CityEntity city);
    MonumentEntity findFirstByPlaceId(String id);
    MonumentEntity findByNameMonument(String name);
    MonumentEntity findFirstByNameMonument(String name);
    List<MonumentEntity> findAll();
    MonumentEntity findByIdMonument(String id);
    @Query(nativeQuery = true, value = "select * from monument me where me.place_Id is null ")
    List<MonumentEntity> findMonumentWhere();
    @Query(nativeQuery = true, value = "select * from monument me order by random() limit 2 ")
    List<MonumentEntity> findMonument();
    @Query(nativeQuery = true, value = "select * from monument me order by random() limit 1 ")
    MonumentEntity findMonumentOne();
    @Query(nativeQuery = true, value = "select * from monument me where UPPER(me.name_monument) like %:name% ")
    List<MonumentEntity> findSubstringMonument(@Param("name") String name);
}
