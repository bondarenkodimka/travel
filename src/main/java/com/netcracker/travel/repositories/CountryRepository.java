package com.netcracker.travel.repositories;

import com.netcracker.travel.entities.CityEntity;
import com.netcracker.travel.entities.CountryEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends CrudRepository<CountryEntity, String> {
    CountryEntity findByNameCountry(String name);
    CountryEntity findByCities(CityEntity name);
}
