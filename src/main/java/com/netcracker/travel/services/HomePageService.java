package com.netcracker.travel.services;

import com.netcracker.travel.dto.*;
import com.netcracker.travel.entities.*;
import com.netcracker.travel.repositories.*;
import com.netcracker.travel.utils.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class HomePageService {
    @Autowired
    private MonumentRepository monumentRepo;
    @Autowired
    private PhotoMonumentRepository photoMonumentRepo;
    @Autowired
    private Converter converter;
    @Autowired
    private UserRepository userRepo;
    @Autowired
    private CommentsHotelRepository commentsHotelRepo;
    @Autowired
    private CommentsMonumentRepository commentsMonumentRepo;

    public PhotoMonumentResponse getMonument() {
//        List<MonumentEntity> all = monumentRepo.findAll();

//        Random random = new Random();
        PhotoMonumentResponse monumentResponse = null;
        boolean stop = false;
        while (!stop) {
            MonumentEntity allOne = monumentRepo.findMonumentOne();
//            MonumentEntity entity = all.get(random.nextInt(all.size()));
            try {
                String replaceInfo = null;
                if (allOne.getPhotoMonument() != null && allOne.getPlaceId() != null) {
                    PhotoMonumentEntity firstByMonumentPhotoM = photoMonumentRepo.findFirstByMonumentPhotoM(allOne);
                    if (allOne.getInformationAboutMonument() != null) {
                        replaceInfo = allOne.getInformationAboutMonument().replace("\\" + "n", "").replace("\\", "");
                    }
                    String replaceName = allOne.getNameMonument().replace("\\" + "n", "").replace("\\", "");
                    if (replaceInfo.equals("\"\"") || replaceInfo == null) {
                        monumentResponse = new PhotoMonumentResponse(replaceName, "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="
                                + firstByMonumentPhotoM.getPhoto().substring(1, firstByMonumentPhotoM.getPhoto().length() - 1)
                                + "&key=AIzaSyB-eceCpqDgw9ate_IDpSEOS9_8gibw9do", allOne.getPlaceId(),
                                null, allOne.getLatitude(), allOne.getLongitude(), converter.fromWorkTimeEntityToWorkTimeResponse(allOne.getWorkTime()), allOne.getStatus().getNameStatus());

                    } else {
                        monumentResponse = new PhotoMonumentResponse(replaceName, "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="
                                + firstByMonumentPhotoM.getPhoto().substring(1, firstByMonumentPhotoM.getPhoto().length() - 1)
                                + "&key=AIzaSyB-eceCpqDgw9ate_IDpSEOS9_8gibw9do", allOne.getPlaceId(),
                                replaceInfo, allOne.getLatitude(), allOne.getLongitude(), converter.fromWorkTimeEntityToWorkTimeResponse(allOne.getWorkTime()), allOne.getStatus().getNameStatus());

                    }
                    stop = true;
                }
            } catch (NullPointerException e) {
                stop = false;
            }
        }
        return monumentResponse;
    }

    public UserResponse getUser() {
        List<UserEntity> users = userRepo.findAll();
        Random random = new Random();
        boolean stop = false;
        UserResponse userResponse = null;
        while (!stop) {
            UserEntity entity = users.get(random.nextInt(users.size()));
            if (entity.getPhoto() != null) {
                userResponse = new UserResponse(entity.getIdUser(), entity.getLogin(), entity.getNickname(), entity.getRole(), entity.getPhoto());
                stop = true;
            }
        }
        return userResponse;
    }

    public CreatePlaceResponse getCreatePlace() {
        List<MonumentEntity> entities = monumentRepo.findMonumentWhere();
        Random random = new Random();
        boolean stop = false;
        CreatePlaceResponse place = null;
        while (!stop) {
            MonumentEntity entity = entities.get(random.nextInt(entities.size()));
            try {
                if (entity.getPhotoMonument().size() != 0) {
                    List<String> category = new ArrayList<>();
                    category.add(entity.getStatus().getNameStatus());
                    place = new CreatePlaceResponse(entity.getNameMonument(), entity.getLongitude(), entity.getLatitude(), entity.getInformationAboutMonument(), category, entity.getCityMonument().getNameCity(),
                            entity.getCityMonument().getCountry().getNameCountry(), entity.getPhotoMonument().get(0).getPhoto(), entity.getIdMonument());
                    stop = true;
                }
            } catch (IndexOutOfBoundsException e) {
                stop = false;
            }
        }
        return place;
    }

    public MonumentPageResponse getCommentMonument() {
        try {
            List<CommentsMonumentEntity> entities = commentsMonumentRepo.findAll();
            Random random = new Random();
            boolean stop = false;
            MonumentPageResponse monumentPageResponse = null;
            while (!stop) {
                CommentsMonumentEntity entity = entities.get(random.nextInt(entities.size()));
                try {
                    monumentPageResponse = new MonumentPageResponse(entity.getUserCommentsM().getIdUser(), entity.getUserCommentsM().getNickname(), entity.getUserCommentsM().getPhoto(), null, entity.getComment(), entity.getMonumentCommentsM().getPlaceId(), entity.getDate());
                    stop = true;
                } catch (IndexOutOfBoundsException e) {
                    stop = false;
                }
            }
            return monumentPageResponse;
        } catch (Exception e) {
            return new MonumentPageResponse("", "", "", null, "", "", null);
        }
    }

    public HotelPageResponse getCommentHotel() {
        List<CommentsHotelEntity> entities = commentsHotelRepo.findAll();
        Random random = new Random();
        boolean stop = false;
        HotelPageResponse hotelPageResponse = null;
        while (!stop) {
            if (entities.size() != 0) {
                CommentsHotelEntity entity = entities.get(random.nextInt(entities.size()));
                try {
                    hotelPageResponse = new HotelPageResponse(entity.getUserCommentsH().getIdUser(), entity.getUserCommentsH().getNickname(), entity.getUserCommentsH().getPhoto(), null, entity.getComment(), entity.getHotelCommentsH().getPlaceId(), entity.getDate());
                    stop = true;
                } catch (IndexOutOfBoundsException e) {
                    stop = false;
                }
            } else {
                return null;
            }
        }
        return hotelPageResponse;
    }
}

