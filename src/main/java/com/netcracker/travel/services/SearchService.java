package com.netcracker.travel.services;

import com.netcracker.travel.dto.GeneralUserAndMonument;
import com.netcracker.travel.dto.PhotoMonumentResponse;
import com.netcracker.travel.dto.UserResponse;
import com.netcracker.travel.entities.CityEntity;
import com.netcracker.travel.entities.MonumentEntity;
import com.netcracker.travel.entities.UserEntity;
import com.netcracker.travel.repositories.CityRepository;
import com.netcracker.travel.repositories.MonumentRepository;
import com.netcracker.travel.repositories.PhotoMonumentRepository;
import com.netcracker.travel.repositories.UserRepository;
import com.netcracker.travel.utils.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SearchService {
    @Autowired
    private MonumentRepository monumentRepo;
    @Autowired
    private UserRepository userRepo;
    @Autowired
    private CityRepository cityRepo;
    @Autowired
    private Converter converter;
    @Autowired
    private PhotoMonumentRepository photoMonumentRepo;
    @Autowired
    private PhotoMonumentService photoMonumentService;

    public List<PhotoMonumentResponse> getMonumentByName(String name) {
        List<PhotoMonumentResponse> result = new ArrayList<>();
//        Iterable<MonumentEntity> all = monumentRepo.findAll();
        try {
            List<MonumentEntity> all = monumentRepo.findSubstringMonument(name.toUpperCase());
            List<MonumentEntity> byNameMonument = new ArrayList<>();
            if (all.size() != 0) {
                for (MonumentEntity entity : all) {
                    if (entity.getNameMonument().substring(1, entity.getNameMonument().length() - 1).toLowerCase().contains(name.toLowerCase())) {
                        byNameMonument.add(entity);
                    }
                }

                try {
                    String replace = null;
                    for (MonumentEntity entity : byNameMonument) {
                        if (!entity.getPhotoMonument().get(0).getPhoto().contains("res.cloudinary.com")) {
                            if (entity.getInformationAboutMonument() != null) {
                                replace = entity.getInformationAboutMonument().replace("\\" + "n", "").replace("\\", "");
                            }
                            PhotoMonumentResponse response = new PhotoMonumentResponse(entity.getNameMonument(), "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="
                                    + entity.getPhotoMonument().get(0).getPhoto().substring(1, entity.getPhotoMonument().get(0).getPhoto().length() - 1)
                                    + "&key=AIzaSyB-eceCpqDgw9ate_IDpSEOS9_8gibw9do", entity.getPlaceId(),
                                    replace, entity.getLatitude(), entity.getLongitude(), converter.fromWorkTimeEntityToWorkTimeResponse(entity.getWorkTime()), entity.getStatus().getNameStatus());
                            result.add(response);
                        } else {
                            if (entity.getInformationAboutMonument() != null) {
                                replace = entity.getInformationAboutMonument().replace("\\" + "n", "").replace("\\", "");
                            }
                            PhotoMonumentResponse response = new PhotoMonumentResponse(entity.getNameMonument(),
                                    entity.getPhotoMonument().get(0).getPhoto(), entity.getIdMonument(),
                                    replace, entity.getLatitude(), entity.getLongitude(), converter.fromWorkTimeEntityToWorkTimeResponse(entity.getWorkTime()), entity.getStatus().getNameStatus());
                            result.add(response);
                        }
                    }
                } catch (Exception e) {
                    String replace = null;
                    for (MonumentEntity entity : byNameMonument) {
                        if (entity.getPlaceId() != null) {
                            if (entity.getInformationAboutMonument() != null) {
                                replace = entity.getInformationAboutMonument().replace("\\" + "n", "").replace("\\", "");
                            }
                            PhotoMonumentResponse response = new PhotoMonumentResponse(entity.getNameMonument(), "https://avatars.mds.yandex.net/get-pdb/1895114/f5ef2db2-2dea-48f7-bcec-c7593611dff8/s375", entity.getPlaceId(),
                                    replace, entity.getLatitude(), entity.getLongitude(), converter.fromWorkTimeEntityToWorkTimeResponse(entity.getWorkTime()), entity.getStatus().getNameStatus());
                            result.add(response);
                        } else {
                            if (entity.getInformationAboutMonument() != null) {
                                replace = entity.getInformationAboutMonument().replace("\\" + "n", "").replace("\\", "");
                            }
                            PhotoMonumentResponse response = new PhotoMonumentResponse(entity.getNameMonument(), "https://avatars.mds.yandex.net/get-pdb/1895114/f5ef2db2-2dea-48f7-bcec-c7593611dff8/s375", entity.getIdMonument(),
                                    replace, entity.getLatitude(), entity.getLongitude(), converter.fromWorkTimeEntityToWorkTimeResponse(entity.getWorkTime()), entity.getStatus().getNameStatus());
                            result.add(response);
                        }
                    }
                }
            } else {
                return null;
            }
        } catch (NullPointerException e){
            return null;
        }
        return result;
    }

    public UserResponse getUserByName(String name) {
        try {
            List<UserEntity> byNickname = userRepo.findSubstr(name.toUpperCase());
            if (byNickname.size() != 0) {
                UserResponse response = new UserResponse(byNickname.get(0).getIdUser(), byNickname.get(0).getLogin(), byNickname.get(0).getNickname(), byNickname.get(0).getRole(), byNickname.get(0).getPhoto());
                return response;
            } else {
                return null;
            }
        } catch (NullPointerException e) {
            return null;
        }
    }

    public UserResponse getUserById(String id) {
        UserEntity byNickname = userRepo.findByIdUser(id);
        UserResponse response = new UserResponse(byNickname.getIdUser(), byNickname.getLogin(), byNickname.getNickname(), byNickname.getRole(), byNickname.getPhoto());
        return response;
    }

    public List<PhotoMonumentResponse> getMonumentByCity(String name) {
        Iterable<CityEntity> all = cityRepo.findAll();
        List<String> id = new ArrayList<>();
        List<MonumentEntity> allByCityMonument = null;
        for (CityEntity entity : all) {
            if (entity.getNameCity().toLowerCase().equals(name.toLowerCase())) {
                allByCityMonument = monumentRepo.findAllByCityMonument(entity);
                break;
            }
        }
        try {
            for (MonumentEntity entity : allByCityMonument) {
                id.add(entity.getPlaceId());
            }
            return photoMonumentService.getPhotosByMonument(id);
        } catch (NullPointerException e) {
            return null;
        }
    }

    public PhotoMonumentResponse getMonumentByPlaceId(String placeId) {
        MonumentEntity firstByPlaceId = monumentRepo.findFirstByPlaceId(placeId);
        PhotoMonumentResponse result = null;
        try {
            String replace = firstByPlaceId.getInformationAboutMonument().replace("\\" + "n", "").replace("\\", "");
            PhotoMonumentResponse response = new PhotoMonumentResponse(firstByPlaceId.getNameMonument(), "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="
                    + firstByPlaceId.getPhotoMonument().get(0).getPhoto().substring(1, firstByPlaceId.getPhotoMonument().get(0).getPhoto().length() - 1)
                    + "&key=AIzaSyB-eceCpqDgw9ate_IDpSEOS9_8gibw9do", firstByPlaceId.getPlaceId(),
                    replace, firstByPlaceId.getLatitude(), firstByPlaceId.getLongitude(), converter.fromWorkTimeEntityToWorkTimeResponse(firstByPlaceId.getWorkTime()), firstByPlaceId.getStatus().getNameStatus());
            result = response;
        } catch (NullPointerException e) {
            String replace = firstByPlaceId.getInformationAboutMonument().replace("\\" + "n", "").replace("\\", "");
            PhotoMonumentResponse response = new PhotoMonumentResponse(firstByPlaceId.getNameMonument(), "https://avatars.mds.yandex.net/get-pdb/1895114/f5ef2db2-2dea-48f7-bcec-c7593611dff8/s375", firstByPlaceId.getPlaceId(),
                    replace, firstByPlaceId.getLatitude(), firstByPlaceId.getLongitude(), converter.fromWorkTimeEntityToWorkTimeResponse(firstByPlaceId.getWorkTime()), firstByPlaceId.getStatus().getNameStatus());
            result = response;
        }
        return result;
    }

    public List<GeneralUserAndMonument> findInSubstring(String substring) {
        List<MonumentEntity> allByNameMonument = monumentRepo.findSubstringMonument(substring.toUpperCase());
        List<UserEntity> substr = userRepo.findSubstr(substring.toUpperCase());
        List<GeneralUserAndMonument> result = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            try {
                if (allByNameMonument.get(i).getPlaceId() != null) {
                    GeneralUserAndMonument monument = new GeneralUserAndMonument(allByNameMonument.get(i).getNameMonument().substring(1, allByNameMonument.get(i).getNameMonument().length() - 1), null, allByNameMonument.get(i).getPlaceId(),
                            null, allByNameMonument.get(i).getLatitude(), allByNameMonument.get(i).getLongitude(), null,
                            allByNameMonument.get(i).getStatus().getNameStatus(), null, null, null, null, null);
                    result.add(monument);
                } else {
                    GeneralUserAndMonument monument = new GeneralUserAndMonument(allByNameMonument.get(i).getNameMonument().substring(1, allByNameMonument.get(i).getNameMonument().length() - 1), null, allByNameMonument.get(i).getIdMonument(),
                            null, allByNameMonument.get(i).getLatitude(), allByNameMonument.get(i).getLongitude(), null,
                            allByNameMonument.get(i).getStatus().getNameStatus(), null, null, null, null, null);
                    result.add(monument);
                }

            } catch (Exception e) {
                e.toString();
            }
            try {
                GeneralUserAndMonument user = new GeneralUserAndMonument(null, null, null, null, null, null,
                        null, null, substr.get(i).getIdUser(), substr.get(i).getLogin(), substr.get(i).getNickname(),
                        substr.get(i).getRole(), null);

                result.add(user);
            } catch (Exception e) {
                e.toString();
            }
        }
        return result;
    }

    public List<PhotoMonumentResponse> getMonumentByTypeWithoutCity(List<String> types, String name){
        List<MonumentEntity> allMonumentsForCity = monumentRepo.findAllByCityMonument(cityRepo.findByNameCity(name));
        List<MonumentEntity> filterMonument = new ArrayList<>();
        for(MonumentEntity monument: allMonumentsForCity){
            for(String type: types)
                if(monument.getStatus().getNameStatus().equals(type)){
                    filterMonument.add(monument);
                }
        }
        return converter.fromMonumentEntityToPhotoMonumentResponse(filterMonument);
    }

}
