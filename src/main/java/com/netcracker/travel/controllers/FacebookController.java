package com.netcracker.travel.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netcracker.travel.config.JwtTokenUtil;
import com.netcracker.travel.entities.RoleEntity;
import com.netcracker.travel.entities.UserEntity;
import com.netcracker.travel.functions.Generetion;
import com.netcracker.travel.model.ApiResponse;
import com.netcracker.travel.model.AuthToken;
import com.netcracker.travel.repositories.RoleRepository;
import com.netcracker.travel.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;

@CrossOrigin(origins = {"http://localhost:4200", "https://dream-journey-website.herokuapp.com", "http://dream-journey-website.herokuapp.com"})
@RestController
@RequestMapping("/sign-in/facebook")
public class FacebookController {

    @Value("${my.baseUrl}")
    private String baseUrl;

    @Value("${my.facebookId}")
    private String facebookId;

    @Value("${my.facebookSecret}")
    private String facebookSecret;

    @Value("${my.facebookUrl}")
    private String facebookUrl;

    @Value("${my.facebookUrlUser}")
    private String facebookUrlUser;

    @Autowired
    private UserService userService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private RoleRepository roleRepository;

    @RequestMapping(method = RequestMethod.GET)
    public ApiResponse createUser(@RequestHeader(value = "code", required = true) String code) throws JsonProcessingException {

        String redirect_uri = baseUrl + "/sign-in/oauth/facebook";
        String access_token;

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> tokenInfo = restTemplate.getForEntity(
                facebookUrl +
                        "client_id="+ facebookId + "&" +
                        "redirect_uri="+ redirect_uri + "&" +
                        "client_secret=" + facebookSecret + "&" +
                        "code=" + code,
                String.class);

        if (tokenInfo.getStatusCodeValue() == 200) {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(tokenInfo.getBody());
            access_token = root.path("access_token").textValue();
        }
        else {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Bad request");
        }

        ResponseEntity<String> userInfo = restTemplate.getForEntity(
                facebookUrlUser +
                        "fields=id,first_name,last_name&" +
                        "access_token=" + access_token,
                String.class);

        if (userInfo.getStatusCodeValue() == 200) {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(userInfo.getBody());
            String id = root.path("id").textValue();
            String first_name = root.path("first_name").textValue();
            String last_name = root.path("last_name").textValue();
            String photo = "https://graph.facebook.com/" + id + "/picture?type=large";

            UserEntity userEntity = userService.findByFacebookId(id);

            if (userEntity != null) {
                final String token = jwtTokenUtil.generateToken(userEntity);
                return new ApiResponse(HttpStatus.OK, "Success", new AuthToken(token, userEntity.getIdUser()));
            } else {
                RoleEntity role = roleRepository.findByNameRole("user");
                if (role == null) {
                    role = new RoleEntity("user");
                    roleRepository.save(role);
                }

                Timestamp timestamp = new Timestamp(System.currentTimeMillis());

                Generetion generetion = new Generetion();
                String nickname = generetion.getAlphaNumericString(15);
                userEntity = userService.findByNickname(nickname);
                while (userEntity != null) {
                    nickname = generetion.getAlphaNumericString(15);
                    userEntity = userService.findByNickname(nickname);
                }
                userEntity = new UserEntity(
                        null,
                        null,
                        nickname,
                        first_name,
                        last_name,
                        role,
                        photo,
                        null,
                        true,
                        timestamp,
                        null,
                        null,
                        id
                );

                userService.save(userEntity);
                final String token = jwtTokenUtil.generateToken(userEntity);
                return new ApiResponse(HttpStatus.OK, "Success", new AuthToken(token, userEntity.getIdUser()));
            }
        }
        else {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Bad request");
        }
    }
}
