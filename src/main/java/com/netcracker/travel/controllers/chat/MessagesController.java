package com.netcracker.travel.controllers.chat;

import com.netcracker.travel.dto.ConverterMessage;
import com.netcracker.travel.dto.MessageRequest;
import com.netcracker.travel.entities.DialogEntity;
import com.netcracker.travel.entities.MessageEntity;
import com.netcracker.travel.repositories.MessageRepository;
import com.netcracker.travel.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;

@CrossOrigin(origins = {"http://localhost:4200", "https://dream-journey-website.herokuapp.com", "http://dream-journey-website.herokuapp.com"})
@RestController
@RequestMapping("/messages")
public class MessagesController {

    @Autowired
    MessageRepository messageRepository;

    @Autowired
    UserRepository userRepository;

    @GetMapping
    public ArrayList<MessageRequest> getMessages(String sender, String recent){
        ArrayList<MessageEntity> messages = new ArrayList<>();
        Iterator<MessageEntity> iterator = messageRepository.findAll().iterator();
        while(iterator.hasNext()){
            messages.add(iterator.next());
        }
        ConverterMessage converterMessage = new ConverterMessage();
        ArrayList<MessageRequest> requests = new ArrayList<>();
        for(int i = 0; i < messages.size(); i++){
            if((messages.get(i).getUserSender().getIdUser().equals(sender) && messages.get(i).getUserRecipient().getIdUser().equals(recent)))
            requests.add(converterMessage.convert(messages.get(i), 1));
            if((messages.get(i).getUserSender().getIdUser().equals(recent) && messages.get(i).getUserRecipient().getIdUser().equals(sender))){
                requests.add(converterMessage.convert(messages.get(i), 2));
            }
        }
        return requests;


    }

    @GetMapping("/dialogs")
    public ArrayList<DialogEntity> getDialogs(String id){
        ArrayList<DialogEntity> dialogs = new ArrayList<>();
        ArrayList<String> receviers = new ArrayList<>();
        Iterator<MessageEntity> iterator = messageRepository.findAll().iterator();
        MessageEntity tmp;
        DialogEntity ent;
        while(iterator.hasNext()){
            tmp = iterator.next();
            if(tmp.getUserSender().getIdUser().equals(id)){
                if(!receviers.contains(tmp.getUserRecipient().getIdUser())){
                    receviers.add(tmp.getUserRecipient().getIdUser());
                    ent = new DialogEntity(id, "", tmp.getUserRecipient().getIdUser(), tmp.getUserRecipient().getNickname(), tmp.getUserRecipient().getName(),
                            tmp.getUserRecipient().getSurname(), tmp.getUserRecipient().getPhoto(), tmp.getContent());
                    if(!dialogs.contains(ent))
                    dialogs.add(ent);
                }
            }
            else if(tmp.getUserRecipient().getIdUser().equals(id)){
                if(!receviers.contains(tmp.getUserSender().getIdUser())){
                    receviers.add(tmp.getUserSender().getIdUser());
                    ent = new DialogEntity(id, "", tmp.getUserSender().getIdUser(), tmp.getUserSender().getNickname(),
                            tmp.getUserSender().getName(), tmp.getUserSender().getSurname(), tmp.getUserSender().getPhoto(), tmp.getContent());
                    if(!dialogs.contains(ent))
                    dialogs.add(ent);
                }
            }
        }
        /*
        String ret = "[";
        for(int j = 0; j < dialogs.size(); j++){
            if(ret.charAt(ret.length()-1) != '[')
                ret+=",";
            ret+= "{ \"recevierid\": \"" + dialogs.get(j).getRecevierId() + "\", \"receviername\": \"" + dialogs.get(j).getRecevierName() + "\", \"lastmsg\": \"" +
                    dialogs.get(j).getLastMsg() + "\" }";
        }
        ret+="]";
        return ret;

         */
        return dialogs;
    }

    @PostMapping("/post")
    public String sendMessage(@RequestParam String sender,
                                      @RequestParam String recipient,
                                      @RequestParam String content) throws InterruptedException {
        //Thread.sleep(250);
        MessageEntity entity = new MessageEntity(
                userRepository.findById(sender).get(),
                userRepository.findById(recipient).get(),
                content,
                Timestamp.valueOf(LocalDateTime.now()),
                false
        );
        messageRepository.save(entity);
        return "200";
    }
}
