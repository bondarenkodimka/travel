package com.netcracker.travel.controllers;

import com.netcracker.travel.dto.PhotoMonumentResponse;
import com.netcracker.travel.entities.*;
import com.netcracker.travel.repositories.*;
import com.netcracker.travel.utils.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200", "https://dream-journey-website.herokuapp.com", "http://dream-journey-website.herokuapp.com"})
@RestController
@RequestMapping("/wish")
public class ListWishMonumentController {

    @Autowired
    UserRepository userRepository;
    @Autowired
    MonumentRepository monumentRepository;
    @Autowired
    ListWishMonumentRepository listWishMonumentRepository;
    @Autowired
    CityRepository cityRepository;
    @Autowired
    private PhotoMonumentRepository photoMonumentRepo;
    @Autowired
    private MonumentRepository monumentRepo;
    @Autowired
    private WorkTimeRepository workTimeRepo;
    @Autowired
    private Converter converter;

    @RequestMapping(value = "/add",method = RequestMethod.GET)
    public void addWishMonument(@RequestParam String userid, @RequestParam String idPlaceMonument) {

        MonumentEntity monumentEntity = monumentRepository.findFirstByPlaceId(idPlaceMonument);
        UserEntity userEntity = userRepository.findByIdUser(userid);

        ListWishMonumentEntity listWishMonumentEntity = new ListWishMonumentEntity(monumentEntity, userEntity);
        listWishMonumentRepository.save(listWishMonumentEntity);
        return;
    }

    @RequestMapping(value = "/search",method = RequestMethod.GET)
    public boolean searchWishMonument(@RequestParam String userid, @RequestParam String idPlaceMonument) {

        MonumentEntity monumentEntity = monumentRepository.findFirstByPlaceId(idPlaceMonument);
        UserEntity userEntity = userRepository.findByIdUser(userid);
        ListWishMonumentEntity tf = listWishMonumentRepository.findByUserMonumentWishAndAndMonumentListWish(userEntity, monumentEntity);
        if (tf != null)
            return true;
        else
            return false;
    }
    @RequestMapping(value = "/delete",method = RequestMethod.GET)
    public boolean deleteWishMonument(@RequestParam String userid, @RequestParam String idPlaceMonument) {
        MonumentEntity monumentEntity = monumentRepository.findFirstByPlaceId(idPlaceMonument);
        UserEntity userEntity = userRepository.findByIdUser(userid);
        ListWishMonumentEntity delWish = listWishMonumentRepository.findByUserMonumentWishAndAndMonumentListWish(userEntity, monumentEntity);
        if (delWish != null) {
            listWishMonumentRepository.delete(delWish);
            return false;
        }
        else
            return true;
    }
    @RequestMapping(value = "/search/city",method = RequestMethod.GET)
    public List<PhotoMonumentResponse> searchByCityListWishMonument(@RequestParam String cityName, @RequestParam String idUser) {
        UserEntity userEntity = userRepository.findByIdUser(idUser);
        CityEntity cityEntity =cityRepository.findByNameCity(cityName);
        List<MonumentEntity> monumentEntitiesByCity = new ArrayList<>();
        List<ListWishMonumentEntity> allWish = listWishMonumentRepository.findAllByUserMonumentWish(userEntity);
        for (ListWishMonumentEntity temp: allWish) {
            if(temp.getMonumentListWish().getCityMonument() == cityEntity)
                monumentEntitiesByCity.add(temp.getMonumentListWish());
        }

        List<PhotoMonumentResponse> result = null;
        Converter converter = new Converter();
        List<PhotoMonumentResponse> listPhoto = new ArrayList<>();

        try {
            for(MonumentEntity temp: monumentEntitiesByCity) {
                try {

                    MonumentEntity byNameMonument = monumentRepo.findFirstByPlaceId(temp.getPlaceId());
                    try {
                        List<WorkTimeEntity> allByMonumentWorkT = workTimeRepo.findAllByMonumentWorkT(byNameMonument);
                        PhotoMonumentEntity byMonumentPhotoM = photoMonumentRepo.findFirstByMonumentPhotoM(byNameMonument);
                        try {
                            PhotoMonumentResponse photoMonumentResponse = new PhotoMonumentResponse(byNameMonument.getNameMonument(), "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="
                                    + byMonumentPhotoM.getPhoto().substring(1, byMonumentPhotoM.getPhoto().length() - 1)
                                    + "&key=AIzaSyB-eceCpqDgw9ate_IDpSEOS9_8gibw9do", temp.getPlaceId(), byNameMonument.getInformationAboutMonument(), byNameMonument.getLatitude(), byNameMonument.getLongitude(),
                                    converter.fromWorkTimeEntityToWorkTimeResponse(allByMonumentWorkT), byNameMonument.getStatus().getNameStatus());
                            listPhoto.add(photoMonumentResponse);
                        } catch (NullPointerException e) {
                            PhotoMonumentResponse photoMonumentResponse = new PhotoMonumentResponse(byNameMonument.getNameMonument(), "https://avatars.mds.yandex.net/get-pdb/1895114/f5ef2db2-2dea-48f7-bcec-c7593611dff8/s375",  temp.getPlaceId(), byNameMonument.getInformationAboutMonument(),
                                    byNameMonument.getLatitude(), byNameMonument.getLongitude(),
                                    converter.fromWorkTimeEntityToWorkTimeResponse(allByMonumentWorkT), byNameMonument.getStatus().getNameStatus());
                            listPhoto.add(photoMonumentResponse);
                        }
                    } catch (NullPointerException e) {
                        MonumentEntity byNameMonument1 = monumentRepo.findFirstByNameMonument( temp.getPlaceId());
                        List<WorkTimeEntity> allByMonumentWorkT = workTimeRepo.findAllByMonumentWorkT(byNameMonument1);
                        PhotoMonumentEntity byMonumentPhotoM = photoMonumentRepo.findFirstByMonumentPhotoM(byNameMonument1);
                        try {
                            PhotoMonumentResponse photoMonumentResponse = new PhotoMonumentResponse(byNameMonument1.getNameMonument(), "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="
                                    + byMonumentPhotoM.getPhoto().substring(1, byMonumentPhotoM.getPhoto().length() - 1)
                                    + "&key=AIzaSyB-eceCpqDgw9ate_IDpSEOS9_8gibw9do",  temp.getPlaceId(), byNameMonument1.getInformationAboutMonument(), byNameMonument1.getLatitude(), byNameMonument1.getLongitude(),
                                    converter.fromWorkTimeEntityToWorkTimeResponse(allByMonumentWorkT), byNameMonument.getStatus().getNameStatus());
                            listPhoto.add(photoMonumentResponse);
                        } catch (NullPointerException c) {
                            PhotoMonumentResponse photoMonumentResponse = new PhotoMonumentResponse(byNameMonument1.getNameMonument(), "https://avatars.mds.yandex.net/get-pdb/1895114/f5ef2db2-2dea-48f7-bcec-c7593611dff8/s375",  temp.getPlaceId(), byNameMonument1.getInformationAboutMonument(),
                                    byNameMonument1.getLatitude(), byNameMonument1.getLongitude(),
                                    converter.fromWorkTimeEntityToWorkTimeResponse(allByMonumentWorkT), byNameMonument.getStatus().getNameStatus());
                            listPhoto.add(photoMonumentResponse);
                        }
                    }
                }catch (NullPointerException e) {
                    MonumentEntity byNameMonument = monumentRepo.findFirstByNameMonument( temp.getPlaceId());
                    try {
                        PhotoMonumentEntity byMonumentPhotoM = photoMonumentRepo.findFirstByMonumentPhotoM(byNameMonument);
                        PhotoMonumentResponse photoMonumentResponse = new PhotoMonumentResponse(byNameMonument.getNameMonument(), byMonumentPhotoM.getPhoto(),  temp.getPlaceId(), byNameMonument.getInformationAboutMonument(),
                                byNameMonument.getLatitude(), byNameMonument.getLongitude(),
                                null, byNameMonument.getStatus().getNameStatus());
                        listPhoto.add(photoMonumentResponse);
                    }catch (NullPointerException c){
                        PhotoMonumentResponse photoMonumentResponse = new PhotoMonumentResponse(byNameMonument.getNameMonument(), "https://avatars.mds.yandex.net/get-pdb/1895114/f5ef2db2-2dea-48f7-bcec-c7593611dff8/s375",  temp.getPlaceId(), byNameMonument.getInformationAboutMonument(),
                                byNameMonument.getLatitude(), byNameMonument.getLongitude(),
                                null, byNameMonument.getStatus().getNameStatus());
                        listPhoto.add(photoMonumentResponse);
                    }
                }
            }
            return listPhoto;
        } catch (NullPointerException e) {
            return listPhoto;
        }
    }
}
