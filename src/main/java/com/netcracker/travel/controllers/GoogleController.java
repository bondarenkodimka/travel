package com.netcracker.travel.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netcracker.travel.config.JwtTokenUtil;
import com.netcracker.travel.entities.RoleEntity;
import com.netcracker.travel.entities.UserEntity;
import com.netcracker.travel.functions.Generetion;
import com.netcracker.travel.model.ApiResponse;
import com.netcracker.travel.model.AuthToken;
import com.netcracker.travel.repositories.RoleRepository;
import com.netcracker.travel.services.UserService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;

@CrossOrigin(origins = {"http://localhost:4200", "https://dream-journey-website.herokuapp.com", "http://dream-journey-website.herokuapp.com"})
@RestController
@RequestMapping("/sign-in/google")
public class GoogleController {

    @Value("${my.baseUrl}")
    private String baseUrl;

    @Value("${my.googleId}")
    private String googleId;

    @Value("${my.googleSecret}")
    private String googleSecret;

    @Value("${my.googleUrl}")
    private String googleUrl;

    @Value("${my.googleUrlUser}")
    private String googleUrlUser;

    @Autowired
    private UserService userService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private RoleRepository roleRepository;

    @RequestMapping(method = RequestMethod.GET)
    public ApiResponse createUser(@RequestHeader(value = "code", required = true) String code) throws JsonProcessingException {

        String redirect_uri = baseUrl + "/sign-in/oauth/google";
        String grant_type = "authorization_code";
        String authorization;

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        map.add("code", code);
        map.add("client_id", googleId);
        map.add("client_secret", googleSecret);
        map.add("redirect_uri", redirect_uri);
        map.add("grant_type", grant_type);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(
                googleUrl, request , String.class);
        if (response.getStatusCodeValue() == 200) {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(response.getBody());
            String token_type = root.path("token_type").textValue();
            String access_token = root.path("access_token").textValue();
            authorization = token_type + " " + access_token;
        }
        else {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Bad request");
        }

        headers = new HttpHeaders();
        headers.set("Authorization", authorization);
        HttpEntity<String> entity = new HttpEntity<String>(headers);

        ResponseEntity<String> userData = restTemplate.exchange(
                googleUrlUser,
                HttpMethod.GET,
                entity,
                String.class);

        if (userData.getStatusCodeValue() == 200) {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(userData.getBody());
            String id = root.path("sub").textValue();
            String first_name = root.path("given_name").textValue();
            String last_name = root.path("family_name").textValue();
            String photo = root.path("picture").textValue();

            UserEntity userEntity = userService.findByGoogleId(id);

            if (userEntity != null) {
                final String token = jwtTokenUtil.generateToken(userEntity);
                return new ApiResponse(HttpStatus.OK, "Success", new AuthToken(token, userEntity.getIdUser()));
            } else {
                RoleEntity role = roleRepository.findByNameRole("user");
                if (role == null) {
                    role = new RoleEntity("user");
                    roleRepository.save(role);
                }

                Timestamp timestamp = new Timestamp(System.currentTimeMillis());

                Generetion generetion = new Generetion();
                String nickname = generetion.getAlphaNumericString(15);
                userEntity = userService.findByNickname(nickname);
                while (userEntity != null) {
                    nickname = generetion.getAlphaNumericString(15);
                    userEntity = userService.findByNickname(nickname);
                }
                userEntity = new UserEntity(
                        null,
                        null,
                        nickname,
                        first_name,
                        last_name,
                        role,
                        photo,
                        null,
                        true,
                        timestamp,
                        null,
                        id,
                        null
                );

                userService.save(userEntity);
                final String token = jwtTokenUtil.generateToken(userEntity);
                return new ApiResponse(HttpStatus.OK, "Success", new AuthToken(token, userEntity.getIdUser()));
            }
        }
        else {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Bad request");
        }
    }
}
