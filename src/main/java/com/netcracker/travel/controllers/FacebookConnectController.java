package com.netcracker.travel.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netcracker.travel.entities.UserEntity;
import com.netcracker.travel.model.ApiResponse;
import com.netcracker.travel.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@CrossOrigin(origins = {"http://localhost:4200", "https://dream-journey-website.herokuapp.com"})
@RestController
@RequestMapping("/connect/facebook")
public class FacebookConnectController {

    @Value("${my.baseUrl}")
    private String baseUrl;

    @Value("${my.facebookId}")
    private String facebookId;

    @Value("${my.facebookSecret}")
    private String facebookSecret;

    @Value("${my.facebookUrl}")
    private String facebookUrl;

    @Value("${my.facebookUrlUser}")
    private String facebookUrlUser;

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public ApiResponse createUser(@RequestHeader(value = "code", required = true) String code,
                                  @RequestHeader(value = "Id", required = true) String id_user) throws JsonProcessingException {

        String redirect_uri = baseUrl + "/connect/oauth/facebook";
        String access_token;

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> tokenInfo = restTemplate.getForEntity(
                facebookUrl +
                        "client_id="+ facebookId + "&" +
                        "redirect_uri="+ redirect_uri + "&" +
                        "client_secret=" + facebookSecret + "&" +
                        "code=" + code,
                String.class);

        if (tokenInfo.getStatusCodeValue() == 200) {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(tokenInfo.getBody());
            access_token = root.path("access_token").textValue();
        }
        else {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Bad request");
        }

        ResponseEntity<String> userInfo = restTemplate.getForEntity(
                facebookUrlUser +
                        "fields=id,first_name,last_name&" +
                        "access_token=" + access_token,
                String.class);

        if (userInfo.getStatusCodeValue() == 200) {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(userInfo.getBody());
            String id = root.path("id").textValue();
            String photo = "https://graph.facebook.com/" + id + "/picture?type=large";

            UserEntity userEntity = userService.findByFacebookId(id);

            if (userEntity != null){
                return new ApiResponse(HttpStatus.CONFLICT, "This facebook account is already registered");
            }

            userEntity = userService.findById(id_user);
            userEntity.setFacebookId(id);
            if (userEntity.getPhoto().equals("")){
                userEntity.setPhoto(photo);
            }
            userService.save(userEntity);
            return new ApiResponse(HttpStatus.OK, "Success");

        }
        else {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Bad request");
        }
    }
}
