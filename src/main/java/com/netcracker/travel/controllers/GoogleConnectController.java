package com.netcracker.travel.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netcracker.travel.entities.UserEntity;
import com.netcracker.travel.model.ApiResponse;
import com.netcracker.travel.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@CrossOrigin(origins = {"http://localhost:4200", "https://dream-journey-website.herokuapp.com"})
@RestController
@RequestMapping("/connect/google")
public class GoogleConnectController {

    @Value("${my.baseUrl}")
    private String baseUrl;

    @Value("${my.googleId}")
    private String googleId;

    @Value("${my.googleSecret}")
    private String googleSecret;

    @Value("${my.googleUrl}")
    private String googleUrl;

    @Value("${my.googleUrlUser}")
    private String googleUrlUser;

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public ApiResponse createUser(@RequestHeader(value = "code", required = true) String code,
                                  @RequestHeader(value = "Id", required = true) String id_user) throws JsonProcessingException {

        String redirect_uri = baseUrl + "/connect/oauth/google";
        String grant_type = "authorization_code";
        String authorization;

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        map.add("code", code);
        map.add("client_id", googleId);
        map.add("client_secret", googleSecret);
        map.add("redirect_uri", redirect_uri);
        map.add("grant_type", grant_type);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(
                googleUrl, request , String.class);
        if (response.getStatusCodeValue() == 200) {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(response.getBody());
            String token_type = root.path("token_type").textValue();
            String access_token = root.path("access_token").textValue();
            authorization = token_type + " " + access_token;
        }
        else {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Bad request");
        }

        headers = new HttpHeaders();
        headers.set("Authorization", authorization);
        HttpEntity<String> entity = new HttpEntity<String>(headers);

        ResponseEntity<String> userData = restTemplate.exchange(
                googleUrlUser,
                HttpMethod.GET,
                entity,
                String.class);

        if (userData.getStatusCodeValue() == 200) {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(userData.getBody());
            String id = root.path("sub").textValue();
            String photo = root.path("picture").textValue();

            UserEntity userEntity = userService.findByGoogleId(id);

            if (userEntity != null){
                return new ApiResponse(HttpStatus.CONFLICT, "This google account is already registered");
            }

            userEntity = userService.findById(id_user);
            userEntity.setGoogleId(id);
            if (userEntity.getPhoto().equals("")){
                userEntity.setPhoto(photo);
            }
            userService.save(userEntity);

            return new ApiResponse(HttpStatus.OK, "Success");
        }
        else {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Bad request");
        }
    }
}
