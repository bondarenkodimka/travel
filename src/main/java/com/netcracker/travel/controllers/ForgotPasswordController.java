package com.netcracker.travel.controllers;

import com.netcracker.travel.email.Sender;
import com.netcracker.travel.entities.UserEntity;
import com.netcracker.travel.functions.Generetion;
import com.netcracker.travel.model.ApiResponse;
import com.netcracker.travel.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = {"http://localhost:4200", "https://dream-journey-website.herokuapp.com"})
@RestController
@RequestMapping("/password-recovery")
public class ForgotPasswordController {

    @Autowired
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    private static Sender sender = new Sender();

    @RequestMapping(method = RequestMethod.GET)
    public ApiResponse passwordRecovery(@RequestHeader(value = "login", required = true) String login) {
        UserEntity userEntity = userService.findByLogin(login);
        if (userEntity == null) {
            return new ApiResponse(HttpStatus.NOT_FOUND, "User with this login not found.");
        }
        Generetion generetion = new Generetion();
        String password = generetion.getAlphaNumericString(30);
        String passwordBCrypt = passwordEncoder.encode(password);
        userEntity.setPassword(passwordBCrypt);
        userService.save(userEntity);

        sender.send(
                "Password update",
                "Hello " + userEntity.getName() + ". Your new password: " + password,
                userEntity.getLogin()
        );

    return new ApiResponse(HttpStatus.OK, "Password sent to you mail.");
    }
}
