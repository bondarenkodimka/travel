package com.netcracker.travel.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netcracker.travel.entities.UserEntity;
import com.netcracker.travel.model.ApiResponse;
import com.netcracker.travel.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@CrossOrigin(origins = {"http://localhost:4200", "https://dream-journey-website.herokuapp.com"})
@RestController
@RequestMapping("/connect/vk")
public class VkConnectController {

    @Value("${my.baseUrl}")
    private String baseUrl;

    @Value("${my.vkId}")
    private String vkId;

    @Value("${my.vkSecret}")
    private String vkSecret;

    @Value("${my.vkUrl}")
    private String vkUrl;

    @Value("${my.vkUrlUser}")
    private String vkUrlUser;

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public ApiResponse createUser(@RequestHeader(value = "code", required = true) String code,
                                  @RequestHeader(value = "Id", required = true) String id_user) throws JsonProcessingException {

        String redirect_uri = baseUrl + "/connect/oauth/vk";
        String access_token;

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> tokenInfo = restTemplate.getForEntity(
                vkUrl +
                        "client_id="+ vkId + "&" +
                        "client_secret=" + vkSecret + "&" +
                        "redirect_uri="+ redirect_uri + "&" +
                        "code=" + code,
                String.class);

        if (tokenInfo.getStatusCodeValue() == 200) {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(tokenInfo.getBody());
            access_token = root.path("access_token").textValue();
        }
        else {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Bad request");
        }

        ResponseEntity<String> userData = restTemplate.getForEntity(
                vkUrlUser + access_token,
                String.class);

        if (userData.getStatusCodeValue() == 200) {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(userData.getBody());
            String id = root.path("response").path(0).path("id").toString();
            String photo = root.path("response").path(0).path("photo_max_orig").textValue();

            UserEntity userEntity = userService.findByVkId(id);

            if (userEntity != null){
                return new ApiResponse(HttpStatus.CONFLICT, "This vk account is already registered");
            }

            userEntity = userService.findById(id_user);
            userEntity.setVkId(id);
            if (userEntity.getPhoto().equals("")){
                userEntity.setPhoto(photo);
            }
            userService.save(userEntity);

            return new ApiResponse(HttpStatus.OK, "Success");
        }
        else {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Bad request");
        }
    }
}
