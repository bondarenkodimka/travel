package com.netcracker.travel.dto;

import java.util.List;

public class PhotoMonumentResponse {
    private String nameMonument;
    private String photo;
    private String placeId;
    private String monumentInfo;
    private Double latitude;
    private Double longitude;
    private List<WorkTimeResponse> workTimeMonument;
    private String nameStatus;

    public PhotoMonumentResponse() {
    }

    public PhotoMonumentResponse(String nameMonument, String photo, String placeId, String monumentInfo, Double latitude,
                                 Double longitude, List<WorkTimeResponse> workTimeMonument, String nameStatus) {
        this.nameMonument = nameMonument;
        this.photo = photo;
        this.placeId = placeId;
        this.monumentInfo = monumentInfo;
        this.latitude = latitude;
        this.longitude = longitude;
        this.workTimeMonument = workTimeMonument;
        this.nameStatus = nameStatus;
    }

    public String getNameStatus() {
        return nameStatus;
    }

    public void setNameStatus(String nameStatus) {
        this.nameStatus = nameStatus;
    }

    public String getNameMonument() {
        return nameMonument;
    }

    public void setNameMonument(String nameMonument) {
        this.nameMonument = nameMonument;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getMonumentInfo() {
        return monumentInfo;
    }

    public void setMonumentInfo(String monumentInfo) {
        this.monumentInfo = monumentInfo;
    }

    public List<WorkTimeResponse> getWorkTimeMonument() {
        return workTimeMonument;
    }

    public void setWorkTimeMonument(List<WorkTimeResponse> workTimeMonument) {
        this.workTimeMonument = workTimeMonument;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
