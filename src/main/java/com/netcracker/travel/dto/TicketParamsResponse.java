package com.netcracker.travel.dto;

import java.util.Date;

public class TicketParamsResponse {

    private  String urlTicket;

    private Date dateDeparture;

    private Date dateArrival;

    private String cityDeparture;

    private String cityArrival;

    public TicketParamsResponse() {
    }

    public TicketParamsResponse(String urlTicket, Date dateDeparture, Date dateArrival, String cityDeparture, String cityArrival) {
        this.urlTicket = urlTicket;
        this.dateDeparture = dateDeparture;
        this.dateArrival = dateArrival;
        this.cityDeparture = cityDeparture;
        this.cityArrival = cityArrival;
    }

    public String getUrlTicket() {
        return urlTicket;
    }

    public void setUrlTicket(String urlTicket) {
        this.urlTicket = urlTicket;
    }

    public Date getDateDeparture() {
        return dateDeparture;
    }

    public void setDateDeparture(Date dateDeparture) {
        this.dateDeparture = dateDeparture;
    }

    public Date getDateArrival() {
        return dateArrival;
    }

    public void setDateArrival(Date dateArrival) {
        this.dateArrival = dateArrival;
    }

    public String getCityDeparture() {
        return cityDeparture;
    }

    public void setCityDeparture(String cityDeparture) {
        this.cityDeparture = cityDeparture;
    }

    public String getCityArrival() {
        return cityArrival;
    }

    public void setCityArrival(String cityArrival) {
        this.cityArrival = cityArrival;
    }
}
