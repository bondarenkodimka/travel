package com.netcracker.travel.dto;

import com.netcracker.travel.utils.Day;

import java.sql.Time;

public class WorkTimeResponse {
    private Day day;
    private Time timeOpen;
    private Time timeClose;

    public WorkTimeResponse() {
    }

    public WorkTimeResponse(Day day, Time timeOpen, Time timeClose) {
        this.day = day;
        this.timeOpen = timeOpen;
        this.timeClose = timeClose;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public Time getTimeOpen() {
        return timeOpen;
    }

    public void setTimeOpen(Time timeOpen) {
        this.timeOpen = timeOpen;
    }

    public Time getTimeClose() {
        return timeClose;
    }

    public void setTimeClose(Time timeClose) {
        this.timeClose = timeClose;
    }
}
