package com.netcracker.travel.dto;

public class MonumentResponse {
    private String nameMonument;
    private Double longitude;
    private Double latitude;
    private String placeId;
    private String link;


    public MonumentResponse() {
    }


    public MonumentResponse(String nameMonument, Double longitude, Double latitude, String placeId, String link) {
        this.nameMonument = nameMonument;
        this.longitude = longitude;
        this.latitude = latitude;
        this.placeId = placeId;
        this.link = link;
    }

    public String getNameMonument() {
        return nameMonument;
    }

    public void setNameMonument(String nameMonument) {
        this.nameMonument = nameMonument;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
