package com.netcracker.travel.dto.forGraf;

import com.netcracker.travel.dto.PhotoMonumentResponse;
import com.netcracker.travel.entities.HotelEntity;
import com.netcracker.travel.entities.MonumentEntity;
import com.netcracker.travel.repositories.HotelRepository;
import com.netcracker.travel.utils.logicGetOptimalHotel.CalculatesOptimalCoordinates;

import java.util.List;

public class WorkWithMatrixDistation {
    private double[][] matrixDistation;
    private int N;

    // для пути, который будем получать через гугл надо переписать, чтобы чекал все, т.е. путь от A to B, и B to A
    public WorkWithMatrixDistation(int N, List<PhotoMonumentResponse> monuments, HotelEntity hotel) {
        this.N = N;
        double distance = 0;
        CalculatesOptimalCoordinates calculate = new CalculatesOptimalCoordinates();
        this.matrixDistation = new double[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = i; j < N; j++) {
                distance = 0;
                if (j == i)
                    this.matrixDistation[i][j] = 0;
                else {
                    if (i == 0) {
                        this.matrixDistation[i][j] = calculate.getDistanceFromLatLonInKm(hotel.getLatitude(), hotel.getLongitude(), monuments.get(i).getLatitude(), monuments.get(i).getLongitude());
                    }
                    else {
                        this.matrixDistation[i][j] = calculate.getDistanceFromLatLonInKm(monuments.get(j).getLatitude(), monuments.get(j).getLongitude(), monuments.get(i).getLatitude(), monuments.get(i).getLongitude());
                    }
                    this.matrixDistation[j][i] = this.matrixDistation[i][j];
                }
            }
        }
    }

    public void show() {
        for (int i = 0; i < N; i++) {
            for (int j = i; j < N; j++) {
                System.out.print(this.matrixDistation[i][j] + ' ');
            }
            System.out.println();
        }
    }
}
